﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerSocket
{
    public partial class Form1 : Form
    {
        private String strHostIP;
        private int port;
        private TcpListener tcpListener;
        private Socket client;

        public Form1()
        {
            InitializeComponent();
            strHostIP = "192.168.0.161";
            port = 8080;
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    e.Effect = DragDropEffects.Copy;
                    var directoryName = (string[])e.Data.GetData(DataFormats.FileDrop);
                    Image fileImg = Image.FromFile(directoryName[0], true);
                    byte[] fileToByte = ImageToByteArray(fileImg);

                    StartSocket(fileToByte);
                }
            }catch (System.Exception a)
            {
                MessageBox.Show(a.Message);
            }
            
            
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if(e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
                //e.Effect = DragDropEffects.None;
            }
        }
        public byte[] ImageToByteArray(Image image)
        {
            MemoryStream ms = new MemoryStream();
            image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

            return ms.ToArray();
        }
        public void StartSocket(byte[] buffer)
        {
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, port);
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            server.Bind(ipep);
            server.Listen(20);

            while (true)
            {
                client = server.Accept();
                IPEndPoint ip = (IPEndPoint)client.RemoteEndPoint;

                SendSocketImage(BitConverter.GetBytes(buffer.Length));
                SendSocketImage(buffer);

                if(ReceiveData(client) == null)
                {
                    continue;
                }
                else if(ReceiveData(client) == "end")
                {
                    client.Close();
                    break;
                }

            }
        }
        public void SendSocketImage(byte[] buffer)
        {
            client.Send(buffer);            
        }
        public string ReceiveData(Socket socket)
        {
            byte[] data = new byte[256];
            socket.Receive(data);

            string returnmsg = Encoding.Default.GetString(data);
            socket.Close();

            return returnmsg;
        }
    }
}
