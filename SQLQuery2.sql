
-- =============================================
-- Author: Lee Woo Bin (wblee@awoo.co.kr)
-- Create date: 2019-11-08
-- Browsable : true
-- Group : 리포트
-- Description:	시간당 생산성조회
-- Modified:
-- =============================================
ALTER PROCEDURE [SMS].[usp_GetChuteProductivityPerTime_DynamicGridView]
	@pSystemCode VARCHAR(20),
	@pProcessUserID VARCHAR (20),
	@pProcessLanguage VARCHAR (20),
	@pFromDate DATE = NULL,
	@pToDate DATE = NULL
WITH EXECUTE AS CALLER
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE	@SystemCode VARCHAR(20) = @pSystemCode,
			@FromDate DATE = CASE WHEN ISNULL(@pFromDate, '') = '' THEN GETDATE() ELSE @pFromDate END,
			@ToDate DATE = CASE WHEN ISNULL(@pToDate, '') = '' THEN DATEADD(DAY, 1, GETDATE()) ELSE DATEADD(DAY, 1, @pToDate) END

	
	(
		SELECT
				'슈트물동량' AS Division,
				CONVERT(DECIMAL(10,3),SUM(SPH.ChuteProcessQty)) AS SumData,
				CONVERT(DECIMAL(10,3), AVG(SUM(SPH.ChuteProcessQty)) OVER (PARTITION BY CRH.JobDate)) AS AvgData
		FROM
				SMS.STB_SmsProcessHistory SPH WITH(NOLOCK)
				LEFT OUTER JOIN SMS.STB_ChuteRunDetail CRD WITH(NOLOCK)
					ON	CRD.SystemCode = SPH.SystemCode AND
						CRD.GoodReceiptNo = SPH.GoodReceiptNo 
				LEFT OUTER JOIN	SMS.STB_ChuteRunHistory CRH WITH(NOLOCK)
					ON	CRH.SystemCode = SPH.SystemCode AND
						CRH.CRHNo = CRD.CRHNo
		--WHERE
		--		SPH.SystemCode = @SystemCode AND
		--		CRH.JobDate >= @FromDate AND
		--		CRH.JobDate < @ToDate
		--GROUP BY
				--CRH.JobDate,
				--SPH.ChuteProcessQty
	) 
	UNION ALL
	(
		SELECT
				'가동시간',
				CONVERT(DECIMAL(10,3),SUM(ROUND(DATEDIFF(MINUTE, CRH.StartDateTime, CRH.EndDateTime)/60.0,1))),
				CONVERT(DECIMAL(10,3),AVG(ROUND(DATEDIFF(MINUTE, CRH.StartDateTime, CRH.EndDateTime)/60.0,1)))
		FROM
				SMS.STB_ChuteRunHistory CRH WITH(NOLOCK)
		WHERE
				CRH.SystemCode = @SystemCode AND
				CRH.JobDate >= @FromDate AND
				CRH.JobDate < @ToDate AND
				CRH.StartDateTime IS NOT NULL
	)
	UNION ALL
	(
		SELECT
				'생산성',
				CONVERT(DECIMAL(10, 3), CONVERT(DECIMAL(10,3),SUM(SPH.ChuteProcessQty))/CONVERT(DECIMAL(10,3),SUM(ROUND(DATEDIFF(MINUTE, CRH.StartDateTime, CRH.EndDateTime)/60.0,1)))),
				CONVERT(DECIMAL(10, 3), CONVERT(DECIMAL(10,3),AVG(SPH.ChuteProcessQty))/CONVERT(DECIMAL(10,3),AVG(ROUND(DATEDIFF(MINUTE, CRH.StartDateTime, CRH.EndDateTime)/60.0,1))))
		FROM
				SMS.STB_SmsProcessHistory SPH WITH(NOLOCK)
				LEFT OUTER JOIN SMS.STB_ChuteRunDetail CRD WITH(NOLOCK)
					ON	CRD.SystemCode = SPH.SystemCode AND
						CRD.GoodReceiptNo = SPH.GoodReceiptNo 
				LEFT OUTER JOIN	SMS.STB_ChuteRunHistory CRH WITH(NOLOCK)
					ON	CRH.SystemCode = SPH.SystemCode AND
						CRH.CRHNo = CRD.CRHNo	
		WHERE
				SPH.SystemCode = @SystemCode AND
				CRH.JobDate >= @FromDate AND
				CRH.JobDate < @ToDate AND
				CRH.StartDateTime IS NOT NULL
	)


	SELECT
			CONVERT(VARCHAR, DATEADD(DAY, SV.number, @FromDate), 23) AS JobDate,
			'INT' AS DataType,
			CONVERT(BIT, 1) AS ReadOnly,
			'일자' AS InputJobDateBand
	FROM
			master.dbo.spt_values SV
	WHERE
			SV.type = 'P' AND
			SV.number < DATEDIFF(DAY, @FromDate, @ToDate)

	--DECLARE @SystemCode VARCHAR(20)= 'ASEEN'
	--DECLARE @FromDate DATE = '2019-11-01'
	--DECLARE @ToDate DATE = '2019-11-11'
	(
		SELECT
				'슈트물동량' AS Division,
				'일자' AS InputJobDateBand,
				CONVERT(VARCHAR, CRH.JobDate, 23) AS JobDate,
				CONVERT(DECIMAL(10,3),SUM(SPH.ChuteProcessQty)) AS InputData
				--CONVERT(DECIMAL(10,3),(CONVERT(FLOAT,SUM(SPH.ChuteProcessQty)))) AS InputData
		FROM
				SMS.STB_ChuteRunHistory CRH WITH(NOLOCK) 
				LEFT OUTER JOIN SMS.STB_ChuteRunDetail CRD WITH(NOLOCK)
					ON	CRD.SystemCode = CRH.SystemCode AND
						CRD.CRHNo = CRH.CRHNo
				LEFT OUTER JOIN SMS.STB_SmsProcessHistory SPH WITH(NOLOCK)
					ON	SPH.SystemCode = CRH.SystemCode AND
						SPH.GoodReceiptNo = CRD.GoodReceiptNo
		WHERE 
				CRH.SystemCode = @SystemCode AND
				CRH.JobDate >= @FromDate AND
				CRH.JobDate < @ToDate
		GROUP BY
				CRH.JobDate
	) UNION ALL
	(
		SELECT
				'가동시간' AS Division,
				'일자' AS InputJobDateBand,
				CONVERT(VARCHAR, CRH.JobDate, 23) AS JobDate,
				CONVERT(DECIMAL(10,3),SUM(ROUND(DATEDIFF(MINUTE, CRH.StartDateTime, CRH.EndDateTime)/60.0,1)))
		FROM
				SMS.STB_ChuteRunHistory CRH WITH(NOLOCK)
		WHERE 
				CRH.SystemCode = @SystemCode AND
				CRH.JobDate >= @FromDate AND
				CRH.JobDate < @ToDate
		GROUP BY
				CRH.JobDate
	) UNION ALL
	(
		SELECT
				'생산성' AS Division,
				'일자' AS InputJobDateBand,
				CONVERT(VARCHAR, CRH.JobDate, 23) AS JobDate,
				--SUM(SPH.ChuteProcessQty) / ROUND(DATEDIFF(MINUTE, CRH.StartDateTime, CRH.EndDateTime)/60,1)
				--CONVERT(DECIMAL(10,3),CONVERT(FLOAT,SUM(SPH.ChuteProcessQty))) / CONVERT(DECIMAL(10,3),CONVERT(FLOAT,DATEDIFF(MINUTE, CRH.StartDateTime, CRH.EndDateTime)/60))
				CONVERT(DECIMAL(10,3),CONVERT(DECIMAL(10,3),SUM(SPH.ChuteProcessQty))/CONVERT(DECIMAL(10,3),SUM(ROUND(DATEDIFF(MINUTE, CRH.StartDateTime, CRH.EndDateTime)/60.0,1))))
		FROM
				SMS.STB_ChuteRunHistory CRH WITH(NOLOCK)
				LEFT OUTER JOIN SMS.STB_ChuteRunDetail CRD WITH(NOLOCK)
					ON	CRD.SystemCode = CRH.SystemCode AND
						CRD.CRHNo = CRH.CRHNo
				LEFT OUTER JOIN SMS.STB_SmsProcessHistory SPH WITH(NOLOCK)
					ON	SPH.SystemCode = CRH.SystemCode AND
						SPH.GoodReceiptNo = CRD.GoodReceiptNo				
		WHERE 
				CRH.SystemCode = @SystemCode AND
				CRH.JobDate >= @FromDate AND
				CRH.JobDate < @ToDate
		GROUP BY
				CRH.JobDate
	)
			
END