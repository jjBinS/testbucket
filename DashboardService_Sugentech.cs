﻿using Awoo.SmartFactory.MobileWeb.Model;
using Blazored.SessionStorage;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static Awoo.SmartFactory.MobileWeb.Pages.Dashboard.DashboardProduction_Sugentech;

namespace Awoo.SmartFactory.MobileWeb.Data
{
    public class DashboardService_Sugentech : ServiceBase
    {
        public DashboardService_Sugentech(IConfiguration configuration, ISessionStorageService sessionStorage, UserInfo user) : base(configuration, sessionStorage, user)
        {
        }
        public async Task<List<DashboardProductionInfo>> GetDashboardProduction(string companyCode, string workCenterCode, string shiftCode, DateTime jobDate, DataContext targetContext, DashboardProductionInfo info)
        {
            List<DashboardProductionInfo> infos = new List<DashboardProductionInfo>();
            switch (targetContext)
            {
                case DataContext.PRODROUTE:
                    infos = await GetDashboardProduction_ProdRoute(companyCode, workCenterCode, shiftCode, jobDate);
                    break;
                case DataContext.LINE:
                    infos = await GetDashboardProduction_Line(companyCode, workCenterCode, shiftCode, jobDate, info.ProductionRouteInfo.Code);
                    break;
                case DataContext.MATERIAL:
                    infos = await GetDashboardProduction_Material(companyCode, workCenterCode, jobDate);
                    break;
                default:
                    break;
            }
            return infos;
        }

        public async Task<List<DashboardProductionInfo>> GetDashboardProduction_ProdRoute(string companyCode, string workCenterCode, string shiftCode, DateTime jobDate)
        {
            using SqlConnection conn = new SqlConnection(GetConnectionString());
            using SqlCommand cmd = new SqlCommand("usp_GetDashboardProduction_ProdRoute", conn);
            using SqlDataAdapter sda = new SqlDataAdapter(cmd);
            cmd.CommandType = CommandType.StoredProcedure;

            await conn.OpenAsync();

            SqlCommandBuilder.DeriveParameters(cmd);

            cmd.Parameters["@pProcessLanguage"].Value = User.Language;
            cmd.Parameters["@pProcessUserID"].Value = User.Id;
            cmd.Parameters["@pSystemCode"].Value = User.SystemCode;
            cmd.Parameters["@pCompanyCode"].Value = companyCode;
            cmd.Parameters["@pWorkCenterCode"].Value = workCenterCode;
            cmd.Parameters["@pShiftCode"].Value = shiftCode;
            cmd.Parameters["@pJobDate"].Value = jobDate;

            DataTable dt = new DataTable();
            sda.Fill(dt);

            List<DashboardProductionInfo> ListOfData = GetProductionInfoListByDataTable(dt);

            return await Task.FromResult(ListOfData);
        }

        public async Task<List<DashboardProductionInfo>> GetDashboardProduction_Line(string companyCode, string workCenterCode, string shiftCode, DateTime jobDate, string productionRouteCode)
        {
            using SqlConnection conn = new SqlConnection(GetConnectionString());
            using SqlCommand cmd = new SqlCommand("MES.usp_GetDashboardProduction_Line", conn);
            using SqlDataAdapter sda = new SqlDataAdapter(cmd);
            cmd.CommandType = CommandType.StoredProcedure;

            await conn.OpenAsync();

            SqlCommandBuilder.DeriveParameters(cmd);

            cmd.Parameters["@pProcessLanguage"].Value = User.Language;
            cmd.Parameters["@pProcessUserID"].Value = User.Id;
            cmd.Parameters["@pSystemCode"].Value = User.SystemCode;
            cmd.Parameters["@pCompanyCode"].Value = companyCode;
            cmd.Parameters["@pWorkCenterCode"].Value = workCenterCode;
            cmd.Parameters["@pShiftCode"].Value = shiftCode;
            cmd.Parameters["@pJobDate"].Value = jobDate;
            cmd.Parameters["@pProductionRouteCode"].Value = productionRouteCode;

            DataTable dt = new DataTable();
            sda.Fill(dt);

            List<DashboardProductionInfo> ListOfData = GetProductionInfoListByDataTable(dt);

            return await Task.FromResult(ListOfData);
        }

        public async Task<List<DashboardProductionInfo>> GetDashboardProduction_Material(string companyCode, string workCenterCode, DateTime jobDate)
        {
            using SqlConnection conn = new SqlConnection(GetConnectionString());
            using SqlCommand cmd = new SqlCommand("MES.usp_GetDashboardProduction_Material", conn);
            using SqlDataAdapter sda = new SqlDataAdapter(cmd);
            cmd.CommandType = CommandType.StoredProcedure;

            await conn.OpenAsync();

            SqlCommandBuilder.DeriveParameters(cmd);

            cmd.Parameters["@pProcessLanguage"].Value = User.Language;
            cmd.Parameters["@pProcessUserID"].Value = User.Id;
            cmd.Parameters["@pSystemCode"].Value = User.SystemCode;
            cmd.Parameters["@pCompanyCode"].Value = companyCode;
            cmd.Parameters["@pWorkCenterCode"].Value = workCenterCode;
            cmd.Parameters["@pJobDate"].Value = jobDate;

            DataTable dt = new DataTable();
            sda.Fill(dt);

            List<DashboardProductionInfo> ListOfData = (from dr in dt.AsEnumerable()
                                                        select new DashboardProductionInfo()
                                                        {
                                                            JobDate = dr.Field<DateTime>("JobDate"),
                                                            WorkCenterInfo = new WorkCenterInfo()
                                                            {
                                                                CompanyCode = dr.Field<string>("CompanyCode"),
                                                                WorkCenterCode = dr.Field<string>("WorkCenterCode"),
                                                                WorkCenterName = dr.Field<string>("WorkCenterName")
                                                            },
                                                            ShiftInfo = new ShiftInfo()
                                                            {
                                                                ShiftCode = "",
                                                                ShiftName = ""
                                                            },
                                                            LineInfo = new LineInfo()
                                                            {
                                                                LineCode = "",
                                                                LineName = "",
                                                            },
                                                            ProductionRouteInfo = new ProductionRouteInfo()
                                                            {
                                                                Code = "",
                                                                Name = "",
                                                            },
                                                            DashboardDataType = dr.Field<string>("DashboardDataType"),
                                                            KeyFieldCode = dr.Field<string>("KeyFieldCode"),
                                                            KeyFieldName = dr.Field<string>("KeyFieldName"), 
                                                            MaterialCode = dr.Field<string>("MaterialCode"),
                                                            MaterialName = dr.Field<string>("MaterialName"),
                                                            PlanQty = dr.Field<decimal?>("PlanQty"),
                                                            TargetQty = dr.Field<decimal?>("TargetQty"),
                                                            ProdQty = dr.Field<decimal?>("ProdQty"),
                                                            AccomplishRate = dr.Field<decimal?>("AccomplishRate"),
                                                            LossQty = dr.Field<decimal?>("LossQty"),
                                                            LossRate = dr.Field<decimal?>("LossRate")//,
                                                            //ProcessRate = dr.Field<decimal?>("ProcessRate")
                                                        }).ToList();

            return await Task.FromResult(ListOfData);
        }

        private List<DashboardProductionInfo> GetProductionInfoListByDataTable(DataTable dataTable)
        {
            return (from dr in dataTable.AsEnumerable()
                    select new DashboardProductionInfo()
                    {
                        JobDate = dr.Field<DateTime>("JobDate"),
                        WorkCenterInfo = new WorkCenterInfo()
                        {
                            CompanyCode = dr.Field<string>("CompanyCode"),
                            WorkCenterCode = dr.Field<string>("WorkCenterCode"),
                            WorkCenterName = dr.Field<string>("WorkCenterName")
                        },
                        DashboardDataType = dr.Field<string>("DashboardDataType"),
                        KeyFieldCode = dr.Field<string>("KeyFieldCode"),
                        KeyFieldName = dr.Field<string>("KeyFieldName"),
                        ProductionRouteInfo = new ProductionRouteInfo()
                        {
                            Code = dr.Field<string>("ProductionRouteCode"),
                            Name = dr.Field<string>("ProductionRouteName"),
                        },
                        LineInfo = new LineInfo()
                        {
                            LineCode = dr.Field<string>("LineCode"),
                            LineName = dr.Field<string>("LineName"),
                        },
                        MaterialCode = dr.Field<string>("MaterialCode"),
                        MaterialName = dr.Field<string>("MaterialName"),
                        PlanQty = dr.Field<decimal?>("PlanQty"),
                        TargetQty = dr.Field<decimal?>("TargetQty"),
                        ProdQty = dr.Field<decimal?>("ProdQty"),
                        AccomplishRate = dr.Field<decimal?>("AccomplishRate"),
                        LossQty = dr.Field<decimal?>("LossQty"),
                        LossRate = dr.Field<decimal?>("LossRate"),
                        ProcessRate = dr.Field<decimal?>("ProcessRate")
                    }).ToList();
        }

        public async Task<List<DashboardPackingInfo>> GetDashboardPacking(string companyCode, string workCenterCode, DateTime jobDate)
        {
            using SqlConnection conn = new SqlConnection(GetConnectionString());
            using SqlCommand cmd = new SqlCommand("usp_GetDashboardPacking_mobile", conn);
            using SqlDataAdapter sda = new SqlDataAdapter(cmd);
            cmd.CommandType = CommandType.StoredProcedure;

            await conn.OpenAsync();

            SqlCommandBuilder.DeriveParameters(cmd);

            cmd.Parameters["@pProcessLanguage"].Value = User.Language;
            cmd.Parameters["@pProcessUserID"].Value = User.Id;
            cmd.Parameters["@pSystemCode"].Value = User.SystemCode;
            cmd.Parameters["@pCompanyCode"].Value = companyCode;
            cmd.Parameters["@pWorkCenterCode"].Value = workCenterCode;
            cmd.Parameters["@pJobDate"].Value = jobDate;

            DataTable dt = new DataTable();
            sda.Fill(dt);

            List<DashboardPackingInfo> ListOfData = (from dr in dt.AsEnumerable()
                                                     select new DashboardPackingInfo()
                                                     {
                                                         JobDate = dr.Field<DateTime>("JobDate"),
                                                         WorkCenterInfo = new WorkCenterInfo()
                                                         {
                                                             CompanyCode = dr.Field<string>("CompanyCode"),
                                                             WorkCenterCode = dr.Field<string>("WorkCenterCode"),
                                                             WorkCenterName = dr.Field<string>("WorkCenterName")
                                                         },
                                                         ReceptionNo = dr.Field<string>("ReceptionNo"),
                                                         CustomerPoNumber = dr.Field<string>("CustomerPoNumber"),
                                                         ShipTo = dr.Field<string>("ShipTo"),
                                                         ProductGroupName = dr.Field<string>("ProductGroupName"),
                                                         MaterialCode = dr.Field<string>("MaterialCode"),
                                                         MaterialName = dr.Field<string>("MaterialName"),
                                                         DeliveryPlanDate = dr.Field<DateTime>("DeliveryPlanDate"),
                                                         SOIQty = dr.Field<int?>("SOIQty"),
                                                         PlanQty = dr.Field<int?>("PlanQty"),
                                                         OutputQty = dr.Field<int?>("OutputQty"),
                                                         ProdRate = dr.Field<decimal?>("ProdRate")
                                                     }).ToList();

            return await Task.FromResult(ListOfData);
        }

        public async Task<List<DashboardGIStockInfo>> GetDashboardGIStock(string companyCode, string workCenterCode, DateTime jobDate)
        {
            using SqlConnection conn = new SqlConnection(GetConnectionString());
            using SqlCommand cmd = new SqlCommand("usp_GetDashboardGIStock_mobile", conn);
            using SqlDataAdapter sda = new SqlDataAdapter(cmd);
            cmd.CommandType = CommandType.StoredProcedure;

            await conn.OpenAsync();

            SqlCommandBuilder.DeriveParameters(cmd);

            cmd.Parameters["@pProcessLanguage"].Value = User.Language;
            cmd.Parameters["@pProcessUserID"].Value = User.Id;
            cmd.Parameters["@pSystemCode"].Value = User.SystemCode;
            cmd.Parameters["@pCompanyCode"].Value = companyCode;
            cmd.Parameters["@pWorkCenterCode"].Value = workCenterCode;
            cmd.Parameters["@pJobDate"].Value = jobDate;

            DataTable dt = new DataTable();
            sda.Fill(dt);

            List<DashboardGIStockInfo> ListOfData = (from dr in dt.AsEnumerable()
                                                     select new DashboardGIStockInfo()
                                                     {
                                                         JobDate = dr.Field<DateTime>("JobDate"),
                                                         WorkCenterInfo = new WorkCenterInfo()
                                                         {
                                                             CompanyCode = dr.Field<string>("CompanyCode"),
                                                             WorkCenterCode = dr.Field<string>("WorkCenterCode"),
                                                             WorkCenterName = dr.Field<string>("WorkCenterName")
                                                         },
                                                         SalesYear = dr.Field<string>("SalesYear"),
                                                         SalesMonth = dr.Field<int?>("SalesMonth"),
                                                         ReceptionNo = dr.Field<string>("ReceptionNo"),
                                                         CustomerPoNumber = dr.Field<string>("CustomerPoNumber"),
                                                         ShipTo = dr.Field<string>("ShipTo"),
                                                         ProductGroupName = dr.Field<string>("ProductGroupName"),
                                                         MaterialCode = dr.Field<string>("MaterialCode"),
                                                         MaterialName = dr.Field<string>("MaterialName"),
                                                         Ship_D_Day = dr.Field<int?>("ShipDDay"),
                                                         SOIQty = dr.Field<int?>("SOIQty"),
                                                         StockQty = dr.Field<int?>("StockQty"),
                                                         DiffQty = dr.Field<int?>("DiffQty"),
                                                         GIQty = dr.Field<int?>("GIQty"),
                                                         StatusCode = dr.Field<string>("StatusCode"),
                                                         StatusName = dr.Field<string>("StatusName")
                                                     }).ToList();

            return await Task.FromResult(ListOfData);
        }

        public async Task<List<DashboardGIStockInfoDetail>> GetDashboardGIStockDetail(string companyCode, string workCenterCode, string receptionNo, string salesYear, int? salesMonth, string materialCode)
        {
            using SqlConnection conn = new SqlConnection(GetConnectionString());
            using SqlCommand cmd = new SqlCommand("usp_GetDashboardGIStockDetail", conn);
            using SqlDataAdapter sda = new SqlDataAdapter(cmd);
            cmd.CommandType = CommandType.StoredProcedure;

            await conn.OpenAsync();

            SqlCommandBuilder.DeriveParameters(cmd);

            cmd.Parameters["@pProcessLanguage"].Value = User.Language;
            cmd.Parameters["@pProcessUserID"].Value = User.Id;
            cmd.Parameters["@pSystemCode"].Value = User.SystemCode;
            cmd.Parameters["@pCompanyCode"].Value = companyCode;
            cmd.Parameters["@pWorkCenterCode"].Value = workCenterCode;
            cmd.Parameters["@pReceptionNo"].Value = receptionNo;
            cmd.Parameters["@pSalesYear"].Value = salesYear;
            cmd.Parameters["@pSalesMonth"].Value = salesMonth;
            cmd.Parameters["@pMaterialCode"].Value = materialCode;

            DataTable dt = new DataTable();
            sda.Fill(dt);

            List<DashboardGIStockInfoDetail> ListOfData = (from dr in dt.AsEnumerable()
                                                           select new DashboardGIStockInfoDetail()
                                                           {
                                                               JobDate = dr.Field<DateTime>("JobDate"),
                                                               PlanQty = dr.Field<decimal?>("PlanQty"),
                                                               OutputQty = dr.Field<decimal?>("OutputQty")
                                                           }).ToList();

            return await Task.FromResult(ListOfData);
        }
    }
}
