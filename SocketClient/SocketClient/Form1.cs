﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocketClient
{
    public partial class Form1 : Form
    {
        private string ip = "192.168.0.161";
        private int port = 8080;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            IPAddress iPAddress = IPAddress.Parse(ip);
            socket.Connect(iPAddress,port);
            ReceiveData(socket);

        }
        public void ReceiveData(Socket socket)
        {
            byte[] lengthanddata = new byte[1024];
            socket.Receive(lengthanddata);

            int fileLength = BitConverter.ToInt32(lengthanddata, 0);

            byte[] data = new byte[fileLength];
            socket.Receive(data);
            pictureLoad(data);

            socket.Close();


        }
        public void pictureLoad(byte[] buffer)
        {
            pictureBox1.Image = (byteArrayToImage(buffer));
            //pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        public Image byteArrayToImage(byte[] data)
        {
            MemoryStream ms = new MemoryStream(data);
            Image returnImage = Image.FromStream(ms);

            return returnImage;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            IPAddress iPAddress = IPAddress.Parse(ip);

            string endmsg = "end";
            byte[] endbuffer = new byte[endmsg.Length];

            endbuffer = Encoding.Default.GetBytes(endmsg);
            socket.Connect(iPAddress, port);

            socket.Send(endbuffer);

            socket.Close();
        }
    }
}
